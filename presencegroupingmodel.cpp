#include "presencegroupingmodel.h"

#include <KTp/Models/accounts-model.h>
#include <KTp/presence.h>


#include <QVariant>
#include <KLocalizedString>

PresenceGroupingModel::PresenceGroupingModel(QAbstractItemModel *parent) :
    AbstractGroupingProxyModel(parent)
{
    m_presenceNames[QString::number(Tp::ConnectionPresenceTypeAvailable)] = KTp::Presence(Tp::Presence(Tp::ConnectionPresenceTypeAvailable, "", "")).displayString();
    m_presenceNames[QString::number(Tp::ConnectionPresenceTypeAway)] = KTp::Presence(Tp::Presence(Tp::ConnectionPresenceTypeAway, "", "")).displayString();
    m_presenceNames[QString::number(Tp::ConnectionPresenceTypeExtendedAway)] = KTp::Presence(Tp::Presence(Tp::ConnectionPresenceTypeExtendedAway, "", "")).displayString();
    m_presenceNames[QString::number(Tp::ConnectionPresenceTypeBusy)] = KTp::Presence(Tp::Presence(Tp::ConnectionPresenceTypeBusy, "", "")).displayString();
    m_presenceNames[QString::number(Tp::ConnectionPresenceTypeOffline)] = KTp::Presence(Tp::Presence(Tp::ConnectionPresenceTypeOffline, "", "")).displayString();
    m_presenceNames[QString::number(Tp::ConnectionPresenceTypeUnknown)] = KTp::Presence(Tp::Presence(Tp::ConnectionPresenceTypeUnknown, "", "")).displayString();
}

QSet<QString> PresenceGroupingModel::groupsForIndex(const QModelIndex &sourceIndex) const
{
    return QSet<QString>() << sourceIndex.data(AccountsModel::PresenceTypeRole).toString();
}

QVariant PresenceGroupingModel::dataForGroup(const QString &group, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return m_presenceNames[group];
    }
    return QVariant();
}
