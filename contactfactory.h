#ifndef CONTACTFACTORY_H
#define CONTACTFACTORY_H

#include <TelepathyQt/ContactFactory>
#include <TelepathyQt/Types>

namespace KTp {
class ContactFactory : public Tp::ContactFactory
{
public:
    static Tp::ContactFactoryPtr create(const Tp::Features &features=Tp::Features());
protected:
    ContactFactory(const Tp::Features &features);
    virtual Tp::ContactPtr construct(Tp::ContactManager *manager, const Tp::ReferencedHandles &handle, const Tp::Features &features, const QVariantMap &attributes) const;
};
}


#endif // CONTACTFACTORY_H
