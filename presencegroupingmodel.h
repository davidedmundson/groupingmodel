#ifndef PRESENCEGROUPINGMODEL_H
#define PRESENCEGROUPINGMODEL_H

#include "abstract-grouping-proxy-model.h"

class PresenceGroupingModel : public AbstractGroupingProxyModel
{
    Q_OBJECT
public:
    PresenceGroupingModel(QAbstractItemModel *source);

    virtual QSet<QString> groupsForIndex(const QModelIndex &sourceIndex) const;
    virtual QVariant dataForGroup(const QString &group, int role) const;
private:
    QMap<QString, QString> m_presenceNames;
};

#endif // PRESENCEGROUPINGMODEL_H
