#ifndef CONTACT_H
#define CONTACT_H

#include <QVariant>
#include <TelepathyQt/Contact>
#include <KTp/presence.h>

//K_GLOBAL_STATIC_WITH_ARGS(KTp::ServiceAvailabilityChecker, s_krfbAvailableChecker,
//                          (QLatin1String("org.freedesktop.Telepathy.Client.krfb_rfb_handler")));

namespace KTp{
class Contact : public Tp::Contact
{
    Q_OBJECT
public:
    explicit Contact(Tp::ContactManager *manager, const Tp::ReferencedHandles &handle, const Tp::Features &requestedFeatures, const QVariantMap &attributes);

    KTp::Presence presence() const;

//      TODO merge this from contacts model
//     /** Returns true if audio calls can be started with this contact*/
//     bool audioCallCapability() const;
//     /** Returns true if video calls can be started with this contact*/
//     bool videoCallCapability() const;
//     /** Returns true if file transfers can be started with this contact*/
//     bool fileTransferCapability() const;
//
//     //Overridden as a workaround for upstream bug https://bugs.freedesktop.org/show_bug.cgi?id=55883
//     QStringList clientTypes() const;

};


typedef Tp::SharedPtr<KTp::Contact> ContactPtr;

}//namespace

Q_DECLARE_METATYPE(KTp::ContactPtr)

#endif // CONTACT_H
