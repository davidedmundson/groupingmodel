#include "contactfactory.h"
#include "contact.h"

Tp::ContactFactoryPtr KTp::ContactFactory::create(const Tp::Features &features) {
    return Tp::ContactFactoryPtr(new KTp::ContactFactory(features));
}

KTp::ContactFactory::ContactFactory(const Tp::Features &features)
    : Tp::ContactFactory(features)
{

}

Tp::ContactPtr KTp::ContactFactory::construct(Tp::ContactManager *manager, const Tp::ReferencedHandles &handle, const Tp::Features &features, const QVariantMap &attributes) const
{
    return Tp::ContactPtr(new KTp::Contact(manager, handle, features, attributes));
}
