#include "groupgroupingmodel.h"

#include <KTp/Models/contacts-model.h>

#include <QVariant>
#include <KLocalizedString>

GroupGroupingModel::GroupGroupingModel(QAbstractItemModel *parent) :
    AbstractGroupingProxyModel(parent)
{
}

QSet<QString> GroupGroupingModel::groupsForIndex(const QModelIndex &sourceIndex) const
{
    QStringList groups = sourceIndex.data(ContactsModel::GroupsRole).value<QStringList>();
    if (groups.isEmpty()) {
        groups.append("_unsorted");
    }

    return groups.toSet();
}

QVariant GroupGroupingModel::dataForGroup(const QString &group, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (group == "_unsorted") {
            return i18n("Unsorted");
        } else {
            return group;
        }
    case ContactsModel::IdRole:
        return group;
    }
    return QVariant();
}
