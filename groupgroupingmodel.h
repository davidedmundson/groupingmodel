#ifndef GROUPGROUPINGMODEL_H
#define GROUPGROUPINGMODEL_H

#include "abstract-grouping-proxy-model.h"

class GroupGroupingModel : public AbstractGroupingProxyModel
{
    Q_OBJECT
public:
    explicit GroupGroupingModel(QAbstractItemModel *parent = 0);
    virtual QSet<QString> groupsForIndex(const QModelIndex &sourceIndex) const;
    virtual QVariant dataForGroup(const QString &group, int role) const;
    
    
};

#endif // GROUPGROUPINGMODEL_H
