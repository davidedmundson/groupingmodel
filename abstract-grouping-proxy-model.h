#ifndef ABSTRACTGROUPINGPROXYMODEL_H
#define ABSTRACTGROUPINGPROXYMODEL_H

#include <QStandardItemModel>

class ProxyNode;
class GroupNode;

class AbstractGroupingProxyModel : public QStandardItemModel
{
    Q_OBJECT
public:
    explicit AbstractGroupingProxyModel(QAbstractItemModel *source);

//protected:
    /** Return a list of all groups this items belongs to. Subclasses must override this*/
    virtual QSet<QString> groupsForIndex(const QModelIndex &sourceIndex) const = 0;
    /** Equivalent of QAbstractItemModel::data() called for a specific group header*/
    virtual QVariant dataForGroup(const QString &group, int role) const = 0;

private slots:
    void onRowsInserted(const QModelIndex &sourceParent, int start, int end);
    void onRowsRemoved(const QModelIndex &sourceParent, int start, int end);
    void onDataChanged(const QModelIndex &sourceTopLeft, const QModelIndex &sourceBottomRight);
    void onModelReset();
    
private:    
    /** Create a new proxyNode appended to the given parent in this model*/
    void addProxyNode(const QModelIndex &sourceIndex, QStandardItem *parent);


    /** Returns the standard Item belonging to a particular group name. Creating one if needed*/
    QStandardItem *itemForGroup(const QString &group);

    QAbstractItemModel *m_source;

    //keep a cache of what groups an item belongs to
    QHash<QPersistentModelIndex, QSet<QString> > m_groupCache;

    //item -> groups
    QMultiHash<QPersistentModelIndex, ProxyNode*> m_proxyMap;
    QHash<QString, QStandardItem*> m_groupMap;
};

#endif // GROUPPROXYMODEL_H
